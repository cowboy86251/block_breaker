# Block Breaker
Just a small project for tkinter in python to help sharpen my skills

Check list

- [x] spawn ball
- [x] ball collision
- [x] wall collision
- [x] ball bounces
- [ ] spawn block
- [ ] ball bounces off of block
- [ ] ball can break block
- [ ] spawn platform
- [ ] ball can bounce off platform
- [ ] ball can go passed floor and only platform to save ball
- [ ] add score system
- [ ] add levels