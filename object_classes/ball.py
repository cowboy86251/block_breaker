from .points import Point

import object_classes as oc

# from .wall import Wall
from math import sin, cos, radians


class Ball:
    ANGLE = 90

    def __init__(self, canvas, x, y, r, color, tag):
        self.canvas = canvas
        self.loc = Point(x, y)
        self.r = r
        self.color = color
        self.tag = tag
        self.min_point = None
        self.max_point = None
        self.vel = Point(5, 5)
        self.get_circle_points()
        self.draw()

    def __repr__(self):
        return f"Ball({self.loc.x}, {self.loc.y})"

    def draw(self):
        r = self.r
        self.item = self.canvas.create_oval(
            self.loc.x - r,
            self.loc.y - r,
            self.loc.x + r,
            self.loc.y + r,
            fill=self.color,
            tag=self.tag,
        )

    def points(self):
        r = self.r / 16
        color = "yellow"
        for p in self.cp:
            Ball(self.canvas, p.x, p.y, r, color, "p1")

    def point_on_circle(self, angle):
        x = self.loc.x + self.r * cos(angle)
        y = self.loc.y + self.r * sin(angle)
        return Point(x, y)

    def update(self):
        self.loc.x += self.vel.x
        self.loc.y += self.vel.y
        self.canvas.delete(self.tag)
        if self.min_point is not None and self.max_point is not None:
            self.get_circle_points()
            for point in self.cp:
                if point.y < self.min_point.y:
                    self.move(Point(1, -1))
                elif point.y > self.max_point.y:
                    self.move(Point(1, -1))
                elif point.x < self.min_point.x:
                    self.move(Point(-1, 1))
                elif point.x > self.max_point.x:
                    self.move(Point(-1, 1))
                self.draw()
        else:
            raise f"Ball has no constraints set."

    def move(self, point):
        self.vel *= point

    def constrain(self, max_x, max_y, min_x, min_y):
        self.max_point = Point(max_x, max_y)
        self.min_point = Point(min_x, min_y)

    def get_circle_points(self):
        self.cp = [
            self.point_on_circle(radians(self.ANGLE * i))
            for i in range(int(360 / self.ANGLE))
        ]
